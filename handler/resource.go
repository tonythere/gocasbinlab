package handler

import (
    "github.com/gin-gonic/gin"
)

// EchoResource an dummy handler that echos any request
func EchoResource(c *gin.Context) {
    rsp := gin.H{
        "path":   c.Request.URL.Path,
        "method": c.Request.Method,
    }

    // headers
    for headerName, h := range c.Request.Header {
        rsp["header:" + headerName] = h
    }

    c.JSON(200, rsp)
}

package midw

import (
	"log"

	"github.com/gin-gonic/gin"
)

// AccessControl ...
type AccessController interface {
	GetAuthorizer() gin.HandlerFunc
}

// Enforcer interface for casbin.Enforcer & siblings
type Enforcer interface {
	Enforce(rvals ...interface{}) (bool, error)
}

type accessControlImpl struct {
	enforcer Enforcer
}

// GetAuthorizer ...
func (ac *accessControlImpl) GetAuthorizer() gin.HandlerFunc {
	return ac.authorize
}

func (ac *accessControlImpl) authorize(c *gin.Context) {

	// object will be sent in x-object header
	sub := c.GetHeader("x-user-id")
	act := c.Request.Method
	obj := c.Request.URL.Path

	log.Printf("checking %s, %s, %s\n", sub, obj, act)

	ok, err := ac.enforcer.Enforce(sub, obj, act)
	if err != nil {
		log.Printf("[error] authorize: %s\n", err)
		c.AbortWithStatusJSON(500, gin.H{"error": "error occurred when authorizing user"})
		return
	}

	if !ok {
		c.AbortWithStatusJSON(403, gin.H{"error": "forbidden"})
		return
	}

	c.Next()
}

// NewAccessControl ...
func NewAccessControl(enforcer Enforcer) AccessController {
	return &accessControlImpl{enforcer: enforcer}
}
